const Persona = require("./persona");

class Personas {
  constructor() {
    this._listado = [];
  }

  // Método para crear persona
  crearPersona(persona = {}) {
    this._listado[persona.id] = persona;
  }


  // getter para recorrer los posts de personas y almacenarlos, va junto con crearPersona()
  get listArr() {
    const listado = [];
    Object.keys(this._listado).forEach(key => {
      const persona = this._listado[key];
      listado.push(persona);
    })

    return listado;
  }

  // Método para mostrar la lista de personas en el parámetro GET
  cargarPersonaFromArray(personas = []) {
    personas.forEach(persona => {
      this._listado[persona.id] = persona;
    })
  }

  
  // Método para eliminar una persona
  borrarPersona(id = '') {
    if (this._listado[id]) {
      delete this._listado[id];
    }
  }
}

module.exports = Personas;