const { request } = require('express');
const { guardarDB, leerDB } = require('../helpers/guardarArchivo');
const Persona = require('../models/persona');
const Personas = require('../models/personas');

const { response } = 'express';
const personas = new Personas();

// GET 
const personaGet = (req = request, res = response) => {
  const { nombres, apellidos,ci,direccion,sexo } = req.query;
  const personaDB = leerDB();

  if (personaDB) {
    personas.cargarPersonaFromArray(personaDB);
  }

  res.json({
    msg: 'el get esta funcionando',

    nombres,
    apellidos,
    ci,
    direccion,
    sexo,
    personaDB
  })

};


//POST

const personaPost = (req, res = response) => {
  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const persona = new Persona(nombres, apellidos, ci , direccion, sexo);

  let personaDB = leerDB()

  if (personaDB) {
    personas.cargarPersonaFromArray(personaDB);
  }

  personas.crearPersona(persona);
  guardarDB(personas.listArr);
  personaDB = leerDB()

  const listado = leerDB();

  if (listado) {
    personas.cargarPersonaFromArray(listado);
  }

  res.json({
    msg: 'el post esta funcionando',
    listado
  })
}

// PUT 
const personaPut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    personas.borrarPersona(id)
  
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci , direccion, sexo);
  
    persona.setID(id)
  
    personas.crearPersona(persona);
    guardarDB(personas.listArr);
  }

  res.json({
    msg: 'El put esta funcionando'
  })
}

//DELETE


const personaDelete = (req, res = response) => {

  const { id } = req.params
  
  if (id) {
    personas.borrarPersona(id);
    guardarDB(personas.listArr)

  }

  res.json({
    msg: 'el delete funciona',
  })
};


module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete
}